using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using XELibrary; // for InputHandler

namespace PS1
{
    /// <summary>
    /// This is a game component that implements IUpdateable.
    /// </summary>
    public class Player : Microsoft.Xna.Framework.GameComponent
    {
        private Vector2 position;
        public Vector2 Position
        {
            get { return position; }
        }

        //private readonly float moveRate;
        private IInputHandler input;
        private SpriteBatch spriteBatch;
        public int numOfTapping = 100;
        public Player(Game game,int id_, Vector2 vector)
            : base(game)
        {
            input = (IInputHandler)game.Services.GetService(typeof(IInputHandler));
            position = vector;

        }

        public void Load(SpriteBatch spriteBatch)
        {
            this.spriteBatch = spriteBatch;
        }

        /// <summary>
        /// Allows the game component to perform any initialization it needs to before starting
        /// to run.  This is where it can query for any required services and load content.
        /// </summary>
        public override void Initialize()
        {
            base.Initialize();
        }


    }
}
