using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using XELibrary;

namespace PS1
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class TapFight : Microsoft.Xna.Framework.Game
    {

        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        Texture2D charactersTexture;

        // Actors
        private Player player1;
        private Player player2;
        private Vector2 player1Pos = new Vector2(800.0f, 200.0f);
        private Vector2 player2Pos = new Vector2(100.0f, 200.0f);
        // Announcements
        String player1WinString;
        String player2winString;

        private SpriteFont announceFont;
        private Vector2 announceFontPos;
        private Vector2 announceFontPlayer1Win;
        private Vector2 announceFontPlayer2Win;
        private float announceFontScale;
        private bool endGame = false;
        private bool player1Win = false;
        private bool player2Win = false;
        private double startTime;
        private double endTime;

        private InputHandler input;
        private int viewportWidth;
        private int viewportHeight;

        public TapFight()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";

            graphics.PreferredBackBufferWidth = 1280;
            graphics.PreferredBackBufferHeight = 720;

            input = new InputHandler(this);
            Components.Add(input);

            player1 = new Player(this,1,player1Pos);
            Components.Add(player1);

            player2 = new Player(this,2, player2Pos);
            Components.Add(player2);


            //timeRemaining = 10.0;
            //timeRemainingPos = new Vector2(50, 50);

            //startTime = 2.0;
            //endTime = 1.5;
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            viewportHeight = graphics.GraphicsDevice.Viewport.Height;
            viewportWidth = graphics.GraphicsDevice.Viewport.Width;

            announceFontPos = new Vector2(viewportWidth / 2, viewportHeight / 2);
            announceFontScale = 0.1f;

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // Load the texture that holds the actual sprites.
            charactersTexture = Content.Load<Texture2D>(@"Textures\Characters");
           // progressBarTexture = Content.Load<Texture2D>(@"Textures\progressBar");


            // Load announce font
            announceFont = Content.Load<SpriteFont>(@"Fonts\AnnouncementFont");
            player1WinString = Content.Load<String>(@"Strings\LitalWon");
            player2winString = Content.Load<String>(@"Strings\AmitWon");

            announceFontPlayer1Win = announceFont.MeasureString(player1WinString) / 2.0f;
            announceFontPlayer2Win = announceFont.MeasureString(player2winString) / 2.0f;
            
            player1.Load(spriteBatch);
            player2.Load(spriteBatch);
        }

 
        protected override void UnloadContent()
        {
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Pink);

            spriteBatch.Begin();

            // Allows the game to exit
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed ||
                Keyboard.GetState().IsKeyDown(Keys.Escape))
                this.Exit();
            
            if (!endGame)
            {
                //if lital's button is pressed taps couter is decreased
                if (input.KeyboardHandler.IsKeyDown(Keys.M))
                    player1.numOfTapping--;
                
                //if Amit's button is pressed taps couter is decreased
                if (input.KeyboardHandler.IsKeyDown(Keys.Z))
                    player2.numOfTapping--;
            }

            // if one of the tap counters has reached to 0 the winning player's
            //boolean changed to true and the endgame boolean changes to true
            if (player1.numOfTapping <= 0)
            {
                player1Win = true;
                endGame = true;
            }
            if (player2.numOfTapping <= 0)
            {
                player2Win = true;
                endGame = true;

            }
            spriteBatch.End();

            base.Update(gameTime);
        }
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Pink);

            spriteBatch.Begin();

            // if one of the player ends the tapping an announcment shows up on the screen and the game ends
            if (player1Win)
                    spriteBatch.DrawString(announceFont, player1WinString, announceFontPos, Color.White, 0.0f, announceFontPlayer1Win, announceFontScale, SpriteEffects.None, 0);
            if (player2Win)
                spriteBatch.DrawString(announceFont, player2winString, announceFontPos, Color.White, 0.0f, announceFontPlayer2Win, announceFontScale, SpriteEffects.None, 0);
            // Draw player1
            spriteBatch.Draw(charactersTexture, player1.Position, new Rectangle(350, 0, 350, 600), Color.Pink);

            // Draw player2
            spriteBatch.Draw(charactersTexture, player2.Position, new Rectangle(0, 0, 350, 600), Color.Pink);
            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
